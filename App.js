/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import { SafeAreaView, Text } from "react-native";

import MyWeb from "./src/components/MyWeb";

const App = () => {
  return (
    <SafeAreaView style={{ flex: 1, flexDirection: "column" }}>
      <MyWeb />
    </SafeAreaView>
  );
};

export default App;
