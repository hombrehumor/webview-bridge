export const preventIosCalloutMenu = `
  function addCSSRule(sheet, selector, rules, index) {
    if("insertRule" in sheet) {
      sheet.insertRule(selector + "{" + rules + "}", index);
    }
    else if("addRule" in sheet) {
      sheet.addRule(selector, rules, index);
    }
  }
  addCSSRule(document.styleSheets[0], "a", "-webkit-touch-callout: none;");
`;

export const initLongPressEvent = `
  (function (window, document) {
    var timer = null;

    var isTouch = (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));

    var mouseDown = isTouch ? 'touchstart' : 'mousedown';
    var mouseUp = isTouch ? 'touchend' : 'mouseup';
    document.addEventListener(mouseDown, function(e) {

        var el = e.target;
        var longPressDelayInMs = parseInt(el.getAttribute('data-long-press-delay') || '1500', 10);
        timer = setTimeout(w3samples_long_press.bind(el), longPressDelayInMs);
    });

    document.addEventListener(mouseUp, function() {
        clearTimeout(timer);
    });
    function w3samples_long_press() {

        this.dispatchEvent(new CustomEvent('long-press', { bubbles: true, cancelable: true }));

        clearTimeout(timer);

    }

  }(window, document));
`;

export const initWebViewBridge = `
  var promiseChain = Promise.resolve();
  var callbacks = {};
  var init = function() {
    const guid = function() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
      }
      return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
    }
    window.webViewBridge = {
      send: function(targetFunc, data, success, error) {
        var msgObj = {
          targetFunc: targetFunc,
          data: data || {}
        };
        if (success || error) {
          msgObj.msgId = guid();
        }
        var msg = JSON.stringify(msgObj);
        promiseChain = promiseChain.then(function () {
          return new Promise(function (resolve, reject) {
            console.log("sending message " + msgObj.targetFunc);
            if (msgObj.msgId) {
              callbacks[msgObj.msgId] = {
                onsuccess: success,
                onerror: error
              };
            }
            window.ReactNativeWebView.postMessage(msg);
            resolve();
          })
        }).catch(function (e) {
          console.error('rnBridge send failed ' + e.message);
        });
      },
    };
    window.addEventListener('message', function(e) {
      console.log("message received from react native");
      var message;
      try {
        message = JSON.parse(e.data)
      }
      catch(err) {
        console.error("failed to parse message from react-native " + err);
        return;
      }
      //trigger callback
      if (message.args && callbacks[message.msgId]) {
        if (message.isSuccessfull) {
          callbacks[message.msgId].onsuccess.apply(null, message.args);
        }
        else {
          callbacks[message.msgId].onerror.apply(null, message.args);
        }
        delete callbacks[message.msgId];
      }
    });
  };
  init();

  function sendData(data) {
      window.webViewBridge.send('', data, function(res) {}, function(err) {});
  }
`;
