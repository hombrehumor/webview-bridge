import React, { Component } from "react";
import { View } from "react-native";
import { WebView } from "react-native-webview";

import {
  preventIosCalloutMenu,
  initLongPressEvent,
  initWebViewBridge,
} from "./utils";

class MyWeb extends Component {
  onWebViewMessage = event => {
    console.log("Message received from webview");
    let msgData = JSON.parse(event.nativeEvent.data);
    console.log(msgData);
  };

  render() {
    const runFirst = `
      ${preventIosCalloutMenu}
      ${initLongPressEvent}
      ${initWebViewBridge}
      document.addEventListener('long-press', function(event) {
        sendData(event.target.textContent || "TEST 123");
      });
    `;
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
        <WebView
          ref={webview => {
            this.myWebView = webview;
          }}
          source={{ uri: "https://google.com" }}
          injectedJavaScript={runFirst}
          onMessage={this.onWebViewMessage}
        />
      </View>
    );
  }
}

export default MyWeb;
