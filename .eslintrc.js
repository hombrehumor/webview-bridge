module.exports = {
  root: true,
  extends: "@react-native-community",
  rules: {
    indent: ["error", 2],
    quotes: [2, "double"],
    "no-trailing-spaces": 2
  }
};
